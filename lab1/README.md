# Лабораторная работа №1

## Цель работы
Освоить базовые приёмы и абстракции функционального программирования: функции, поток управления и поток данных, сопоставление с образцом, рекурсия, свёртка, отображение, работа с функциями как с данными, списки.

В рамках лабораторной работы предлагается решить несколько задач [проекта Эйлер](https://projecteuler.net/archives).

## Вариант

12. Highly Divisible Triangular Number
19. Counting Sundays

## 12. Highly Divisible Triangular Number

### Условие

The sequence of triangle numbers is generated by adding the natural numbers. So the 
7-th triangle number would be `1 + 2 + 3 + 4 + 5 + 6 + 7 = 28`. The first ten terms would be:

`1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...`

Let us list the factors of the first seven triangle numbers:

```
1: 1
3: 1, 3
6: 1, 2, 3, 6
10: 1, 2, 5, 10
15: 1, 3, 5, 15
21: 1, 3, 7, 21
28: 1, 2, 4, 7, 14, 28
```


We can see that `28` is the first triangle number to have over five divisors.

What is the value of the first triangle number to have over five hundred divisors?

### Решение

**Примечание**. Треугольное число представляется в виде `n * (n + 1) // 2`.

Я намеренно не использовал эту формулу, чтобы немного усложнить себе жизнь.

#### Реализация на Python

Сперва рассмотрим реализацию на традиционном языке, чтобы было представление об используемом ниже алгоритме.

```python
def count_divisors(n: int) -> int:
    if n == 1:
        return 1

    result: int = 2
    for divisor in range(2, int(n ** .5) + 1):
        if n % divisor != 0:
            continue

        if n // divisor == divisor:
            result += 1
        else:
            result += 2

    return result


def find_triangle_with_divisors(divisor_count: int) -> int:
    n, triangle = 1, 0
    while True:
        triangle += n
        if count_divisors(triangle) > divisor_count:
            return triangle
        n += 1


divisor_count = 500
result = find_triangle_with_divisors(divisor_count)
print(f"The first triangle number with over {divisor_count} divisors is {result}")
```

#### Хвостовая рекурсия

```elixir
defmodule TriangleNumber do
  defp _count_divisors(n, divisor, count) when divisor * divisor > n do
    count
  end
  defp _count_divisors(n, divisor, count) when rem(n, divisor) != 0 do
    _count_divisors(n, divisor + 1, count)
  end
  defp _count_divisors(n, divisor, count) when div(n, divisor) == divisor do
    _count_divisors(n, divisor + 1, count + 1)
  end
  defp _count_divisors(n, divisor, count) do
    _count_divisors(n, divisor + 1, count + 2)
  end

  def count_divisors(n) do
    _count_divisors(n, 1, 0)
  end

  def find_triangle_with_divisors(divisor_count, n \\ 1, sum \\ 0) do
    if count_divisors(sum) > divisor_count do
      sum
    else
      find_triangle_with_divisors(divisor_count, n + 1, sum + n)
    end
  end
end

divisor_count = 500
result = TriangleNumber.find_triangle_with_divisors(divisor_count)
IO.puts("The first triangle number with over #{divisor_count} divisors is #{result}")
```

#### Обычная рекурсия

Здесь меняется только функция `count_divisors`. Не придумал, как можно с помощью обычной рекурсии сделать генерацию последовательности треугольных чисел.

```elixir
defmodule TriangleNumber do
  defp _count_divisors(n, divisor) when divisor * divisor > n do
    1
  end
  defp _count_divisors(n, divisor) when rem(n, divisor) != 0 do
    _count_divisors(n, divisor + 1)
  end
  defp _count_divisors(n, divisor) when div(n, divisor) == divisor do
    1 + _count_divisors(n, divisor + 1)
  end
  defp _count_divisors(n, divisor) do
    2 + _count_divisors(n, divisor + 1)
  end
  defp count_divisors(n) do
    _count_divisors(n, 1)
  end

  def find_triangle_with_divisors(divisor_count, n \\ 1, sum \\ 0) do
    if count_divisors(sum) > divisor_count do
      sum
    else
      find_triangle_with_divisors(divisor_count, n + 1, sum + n)
    end
  end
end

divisor_count = 500
result = TriangleNumber.find_triangle_with_divisors(divisor_count)
IO.puts("The first triangle number with over #{divisor_count} divisors is #{result}")
```

#### Модульная реализация

Здесь выделены следующие модули:

1. Генерация последовательности делителей числа X
2. Подсчет количества делителей в последовательности с помощью `Enum.count`
3. Получение n-ного треугольного числа с помощью `Enum.reduce`

```elixir
defmodule TriangleNumber do
  defp divisors(1), do: [1]
  defp divisors(n), do: [1, n | divisors(2, n, n)]
  defp divisors(k, _n, q) when k * k > q, do: []
  defp divisors(k, n, q) when rem(n, k) != 0 do
    divisors(k + 1, n, q)
  end
  defp divisors(k, n, q) when k * k == n do
    [k | divisors(k + 1, n, q)]
  end
  defp divisors(k, n, q) do
    [k, div(n, k) | divisors(k + 1, n, q)]
  end

  defp count_divisors(n) do
    n
    |> divisors
    |> Enum.count
  end

  defp triangle_number(n) do
    Enum.reduce(1..n, 0, fn x, acc -> x + acc end)
  end

  def find_triangle_with_divisors(divisor_count, n \\ 1) do
    if count_divisors(triangle_number(n)) > divisor_count do
      triangle_number(n)
    else
      find_triangle_with_divisors(divisor_count, n + 1)
    end
  end
end

divisor_count = 500
result = TriangleNumber.find_triangle_with_divisors(divisor_count)
IO.puts("The first triangle number with over #{divisor_count} divisors is #{result}")
```

#### Генерация последовательности при помощи отображения / работа с бесконечными списками

```elixir
defmodule TriangleNumber do
  defp divisors(1), do: [1]
  defp divisors(n), do: [1, n | divisors(2, n, n)]
  defp divisors(k, _n, q) when k * k > q, do: []
  defp divisors(k, n, q) when rem(n, k) != 0 do
    divisors(k + 1, n, q)
  end
  defp divisors(k, n, q) when k * k == n do
    [k | divisors(k + 1, n, q)]
  end
  defp divisors(k, n, q) do
    [k, div(n, k) | divisors(k + 1, n, q)]
  end

  defp count_divisors(n) do
    n
    |> divisors
    |> Enum.count
  end

  defp triangles do
    {0, 1}
    |> Stream.iterate(fn {cur, n} -> {cur + n, n + 1} end)
    |> Stream.map(fn {triangle, _} -> triangle end)
  end

  def find_triangle_with_divisors(divisor_count) do
    triangles()
    |> Stream.map(fn triangle -> {triangle, count_divisors(triangle)} end)
    |> Stream.filter(fn {_triangle, divisors} -> divisors > divisor_count end)
    |> Enum.take(1)
    |> Enum.at(0)
    |> elem(0)
  end
end

divisor_count = 500
result = TriangleNumber.find_triangle_with_divisors(divisor_count)
IO.puts("The first triangle number with over #{divisor_count} divisors is #{result}")
```

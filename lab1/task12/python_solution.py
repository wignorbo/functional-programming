def count_divisors(n: int) -> int:
    if n == 1:
        return 1

    result: int = 2
    for divisor in range(2, int(n ** .5) + 1):
        if n % divisor != 0:
            continue

        if n // divisor == divisor:
            result += 1
        else:
            result += 2

    return result


def find_triangle_with_divisors(divisor_count: int) -> int:
    n, triangle = 1, 0
    while True:
        triangle += n
        if count_divisors(triangle) > divisor_count:
            return triangle
        n += 1


divisor_count = 500
result = find_triangle_with_divisors(divisor_count)
print(f"The first triangle number with over {divisor_count} divisors is {result}")

